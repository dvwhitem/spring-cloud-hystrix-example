package com.hs.controller;

import com.hs.domain.Message;
import com.hs.service.fallback.FallbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vitaliy on 10/29/16.
 */
@RestController
public class FallbackController {

    private FallbackService fallbackService;

    @Autowired
    public FallbackController(FallbackService fallbackService) {
        this.fallbackService = fallbackService;
    }

    @RequestMapping("/message")
    public Message getMessage() {
        return fallbackService.getMessage();
    }
}
