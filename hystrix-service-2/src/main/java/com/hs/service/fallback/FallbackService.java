package com.hs.service.fallback;

import com.hs.domain.Message;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.stereotype.Service;

/**
 * Created by vitaliy on 10/29/16.
 */
@Service
public class FallbackService {

    @HystrixCommand(fallbackMethod = "getNextMessage")
    public Message getMessage() {
        throw new RuntimeException("some message error");
    }

    public Message getNextMessage() {
        return new Message(1, "Some message");
    }
}
