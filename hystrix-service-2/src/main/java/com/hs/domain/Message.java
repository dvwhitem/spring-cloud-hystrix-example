package com.hs.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by vitaliy on 10/29/16.
 */
@Data
@AllArgsConstructor
public class Message {

    private Integer id;
    private String message;
}
