package com.hsc.collapser;

import com.hsc.domain.User;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCollapser;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.stereotype.Service;
import rx.Observable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

/**
 * Created by vitaliy on 10/31/16.
 */
@Service
public class CollapserService {

    /** Asynchronous Execution */
    @HystrixCollapser(scope = com.netflix.hystrix.HystrixCollapser.Scope.GLOBAL, batchMethod = "getUserByIds",
            collapserProperties = {
                @HystrixProperty(name = "timerDelayInMilliseconds", value = "6000")
    })
    public Future<User> getUserByIdAsync(String id) {
        throw new RuntimeException("This method body should not be executed");
    }

    /** Reactive Execution */
    @HystrixCollapser(batchMethod = "getUserByIds",
            collapserProperties = {
                    @HystrixProperty(name = "timerDelayInMilliseconds", value = "6000")
            })
    public Observable<User> getUserByIdReact(String id) {
        throw new RuntimeException("This method body should not be executed");
    }

    @HystrixCommand
    public List<User> getUserByIds(List<String> ids) {
        System.out.println("ID : " + ids);
        List<User> users = new ArrayList<User>();
        for (String id : ids) {
            users.add(new User(id, "name: " + id));
        }
        return users;
    }

}
