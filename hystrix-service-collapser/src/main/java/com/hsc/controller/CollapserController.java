package com.hsc.controller;

import com.google.common.collect.Lists;
import com.hsc.collapser.CollapserService;
import com.hsc.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rx.Observable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by vitaliy on 10/31/16.
 */
@RestController
public class CollapserController {

    CollapserService collapserService;

    @Autowired
    public CollapserController(CollapserService collapserService) {
        this.collapserService = collapserService;
    }

    @RequestMapping(value = "/user/{id}")
    public void getUser(@PathVariable String id) {
        // Async
        Future<User> f1 = collapserService.getUserByIdAsync(id);
        Future<User> f2 = collapserService.getUserByIdAsync("2");
        Future<User> f3 = collapserService.getUserByIdAsync("3");
        Future<User> f4 = collapserService.getUserByIdAsync("4");
        Future<User> f5 = collapserService.getUserByIdAsync("5");
//
        // Reactive
        Observable<User> u1 = collapserService.getUserByIdReact(id);
        Observable<User> u2 = collapserService.getUserByIdReact("2");
        Observable<User> u3 = collapserService.getUserByIdReact("3");
        Observable<User> u4 = collapserService.getUserByIdReact("4");
        Observable<User> u5 = collapserService.getUserByIdReact("5");

        // Materialize reactive commands
        //Iterable<User> users = Observable.merge(u1, u2, u3, u4, u5).toBlocking().toIterable();
    }
}
