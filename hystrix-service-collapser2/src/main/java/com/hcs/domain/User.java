package com.hcs.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by vitaliy on 11/2/16.
 */
@Data
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class User {

    private String id;
    private String name;
}
