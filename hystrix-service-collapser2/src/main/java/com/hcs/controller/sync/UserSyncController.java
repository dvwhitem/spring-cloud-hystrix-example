package com.hcs.controller.sync;

import com.hcs.domain.User;
import com.hcs.service.sync.UserServiceSync;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vitaliy on 11/2/16.
 */
@RestController
public class UserSyncController {

    private UserServiceSync userService;

    @Autowired
    public UserSyncController(UserServiceSync userService) {
        this.userService = userService;
    }

    @RequestMapping("/user-collapser-sync")
    public void getUserCollapser() {
        Long start;

        start = System.currentTimeMillis();
        User u1 = userService.getUser("1");
        System.out.println("Time for u1  " +
                (System.currentTimeMillis() - start) + " ms");


        start = System.currentTimeMillis();
        User u2 = userService.getUser("2");
        System.out.println("Time for u2  " +
                (System.currentTimeMillis() - start) + " ms");


        start = System.currentTimeMillis();
        User u3 = userService.getUser("3");
        System.out.println("Time for u3 " +
                (System.currentTimeMillis() - start) + " ms");

        start = System.currentTimeMillis();
        User u4 = userService.getUser("4");
        System.out.println("Time for u4  " +
                (System.currentTimeMillis() - start) + " ms");

        start = System.currentTimeMillis();
        User u5 = userService.getUser("5");
        System.out.println("Time for u5  " +
                (System.currentTimeMillis() - start) + " ms");

        System.out.println("ID : " + u1.getId() +  "NAME : " + u1.getName());
        System.out.println("ID : " + u2.getId() +  "NAME : " + u2.getName());
        System.out.println("ID : " + u3.getId() +  "NAME : " + u3.getName());
        System.out.println("ID : " + u4.getId() +  "NAME : " + u4.getName());
        System.out.println("ID : " + u5.getId() +  "NAME : " + u5.getName());
    }
}
