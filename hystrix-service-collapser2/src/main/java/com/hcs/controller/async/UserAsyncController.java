package com.hcs.controller.async;

import com.hcs.domain.User;
import com.hcs.service.async.UserServiceAsync;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by vitaliy on 11/2/16.
 */
@RestController
public class UserAsyncController {

    private UserServiceAsync userService;

    @Autowired
    public UserAsyncController(UserServiceAsync userServiceAsync) {
        this.userService = userServiceAsync;
    }

    @RequestMapping("/user-collapser-async")
    public void getUserCollapser() throws ExecutionException, InterruptedException {
        Long start;

        start = System.currentTimeMillis();
        Future<User> u1 = userService.getUser("1");
        System.out.println("Time for u1  " +
                (System.currentTimeMillis() - start) + " ms");


        start = System.currentTimeMillis();
        Future<User> u2 = userService.getUser("2");
        System.out.println("Time for u2  " +
                (System.currentTimeMillis() - start) + " ms");


        start = System.currentTimeMillis();
        Future<User> u3 = userService.getUser("3");
        System.out.println("Time for u3 " +
                (System.currentTimeMillis() - start) + " ms");

        start = System.currentTimeMillis();
        Future<User> u4 = userService.getUser("4");
        System.out.println("Time for u4  " +
                (System.currentTimeMillis() - start) + " ms");

        start = System.currentTimeMillis();
        Future<User> u5 = userService.getUser("5");
        System.out.println("Time for u5  " +
                (System.currentTimeMillis() - start) + " ms");

        System.out.println("ID : " + u1.get().getId() +  "NAME : " + u1.get().getName());
        System.out.println("ID : " + u2.get().getId() +  "NAME : " + u2.get().getName());
        System.out.println("ID : " + u3.get().getId() +  "NAME : " + u3.get().getName());
        System.out.println("ID : " + u4.get().getId() +  "NAME : " + u4.get().getName());
        System.out.println("ID : " + u5.get().getId() +  "NAME : " + u5.get().getName());
    }
}
