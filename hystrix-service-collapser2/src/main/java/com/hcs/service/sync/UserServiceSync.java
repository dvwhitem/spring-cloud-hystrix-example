package com.hcs.service.sync;

import com.hcs.domain.User;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCollapser;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.stereotype.Service;
import static com.netflix.hystrix.contrib.javanica.conf.HystrixPropertiesManager.MAX_REQUESTS_IN_BATCH;
import static com.netflix.hystrix.contrib.javanica.conf.HystrixPropertiesManager.TIMER_DELAY_IN_MILLISECONDS;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vitaliy on 11/2/16.
 */
@Service
public class UserServiceSync {


    @HystrixCollapser(
            scope = com.netflix.hystrix.HystrixCollapser.Scope.GLOBAL,
            batchMethod = "getUsersSync",
            collapserKey = "GetUserCollapser",
            collapserProperties = {
                    @HystrixProperty(name = TIMER_DELAY_IN_MILLISECONDS, value = "3000"),
                    @HystrixProperty(name = MAX_REQUESTS_IN_BATCH, value = "1")
            }
    )
    public User getUser(String id) {
        throw new RuntimeException("this body never call");
    }

    @HystrixCommand
    public List<User> getUsersSync(List<String> ids) {
        System.out.println("ID : " + ids);
        List<User> users = new ArrayList<User>();
        for (String id  : ids) {
                users.add(new User(id, "name " + id));
        }
        return users;
    }
}