package com.hcs.service.async;

import com.hcs.domain.User;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCollapser;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.netflix.hystrix.contrib.javanica.conf.HystrixPropertiesManager;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

/**
 * Created by vitaliy on 11/2/16.
 */
@Service
public class UserServiceAsync {


    @HystrixCollapser(
            batchMethod = "getUsersAsync",
            collapserKey = "getUserCollapser",
            scope = com.netflix.hystrix.HystrixCollapser.Scope.GLOBAL,
            collapserProperties = {
                    @HystrixProperty(name = HystrixPropertiesManager.TIMER_DELAY_IN_MILLISECONDS, value = "8000"),
            }
    )
    public Future<User> getUser(String id) {
        throw new RuntimeException("this method is never call");
    }

    @HystrixCommand
    public List<User> getUsersAsync(List<String> ids) {
        System.out.println("ID : " + ids);
        List<User> users = new ArrayList<>();
        for (String id: ids) {
            users.add(new User(id, "name" + id));
        }
        return users;
    }
}
