package com.hs.controller;

import com.hs.domain.MessageWrapper;
import com.hs.service.CustomerCollapserService;
import com.hs.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;

/**
 * Created by vitaliy on 10/23/16.
 */
@RestController
public class HystrixCollapserController {

    private CustomerCollapserService collapserService;

    @Autowired
    public void setService(CustomerCollapserService service) {
        this.collapserService = service;
    }

    @RequestMapping(value = "/customer-collapser/{id}", produces = "application/json")
    public MessageWrapper getCustomer(@PathVariable Integer id) throws ExecutionException, InterruptedException {
        return collapserService.getCustomerById(id).get();
    }
}
