package com.hs.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

/**
 * Created by vitaliy on 10/23/16.
 */
@RestController
public class HomeController {

    @RequestMapping("/home")
    public List<String> getHomePage() {
        return Arrays.asList("Hello", "World!");
    }
}
