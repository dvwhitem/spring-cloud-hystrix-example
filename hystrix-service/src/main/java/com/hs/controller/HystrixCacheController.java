package com.hs.controller;

import com.hs.domain.Customer;
import com.hs.service.CustomerService;
import com.netflix.hystrix.contrib.javanica.cache.annotation.CacheKey;
import com.netflix.hystrix.strategy.concurrency.HystrixRequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vitaliy on 10/23/16.
 */
@RestController
public class HystrixCacheController {

    CustomerService service;

    @Autowired
    public void setService(CustomerService service) {
        this.service = service;
    }

    @RequestMapping(value = "/customer-cache/{id}", produces = "application/json")
    public Customer getCustomer(@CacheKey @PathVariable Integer id, @RequestParam String name) {
        try {
            HystrixRequestContext.initializeContext();
            return service.createCustomer(id, name);
        } finally {
            HystrixRequestContext.getContextForCurrentThread().shutdown();
        }
    }
}
