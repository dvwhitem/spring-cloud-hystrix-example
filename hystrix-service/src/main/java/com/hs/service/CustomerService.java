package com.hs.service;

import com.hs.domain.Customer;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.cache.annotation.CacheKey;
import com.netflix.hystrix.contrib.javanica.cache.annotation.CacheResult;
import org.springframework.stereotype.Service;

/**
 * Created by vitaliy on 10/23/16.
 */
@Service
public class CustomerService {

    @CacheResult
    @HystrixCommand
    public Customer createCustomer(@CacheKey Integer id, String name) {
        return new Customer(id, name, name);
    }
}
