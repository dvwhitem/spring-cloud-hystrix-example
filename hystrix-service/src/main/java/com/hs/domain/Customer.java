package com.hs.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by vitaliy on 10/23/16.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Customer {

        private Integer id;
        private String firstName;
        private String lastName;
}
