package com.hs.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by vitaliy on 10/23/16.
 */
@Data
@AllArgsConstructor
public class MessageWrapper<T> {

    private T wrapped;
    private String message;
}
